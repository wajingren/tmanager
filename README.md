# tmanager

## tmanager 是一个Tomcat web远程管理汉化版应用

Tomcat提供了manager，可以对应用进行管理，不过manager是英文版，界面只是适配了PC端，操作上也不太流畅。因此，tmanager就出现了。


## manager包括什么
1. 基于Tomcat catalina，可以对应用进行管理（部署、卸载、停止、启动）。
2. 前端使用Amaze UI开发，适配主流浏览器，兼容移动端。
3. 支持实时查看日志

----2017年12月14日17:14:49 by gu-S 

---

全部功能已完成。
经简单测试，操作系统支持Windows、CentOS。支持Tomcat7和Tomcat8,Tomcat6未测试。


效果图如下：

![输入图片说明](https://gitee.com/uploads/images/2018/0103/150010_fee79a84_439973.png "1.png")


![输入图片说明](https://gitee.com/uploads/images/2018/0103/150031_5653bbed_439973.png "2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0103/150042_c57c90a0_439973.png "3.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0103/150054_d1a9c17d_439973.png "4.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0103/150105_3f748f68_439973.png "5.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0103/150114_329e6321_439973.png "6.png")

---

